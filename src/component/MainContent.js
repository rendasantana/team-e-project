import React, { useEffect } from "react";
import { Layout, Typography, Tabs, Card, List } from "antd";
import "antd/dist/antd.css";
import "../asset/style/MainPage.scss";
import { useSelector, useDispatch } from "react-redux";
import { getAllMovies } from "../store/actions/moviesAct";
import { Link } from "react-router-dom";

const { Content } = Layout;
const { Title } = Typography;
const { TabPane } = Tabs;
const { Meta } = Card;

const MainContent = () => {
  const movies = useSelector((state) => state.moviesRed.movies);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllMovies());
  }, [dispatch]);

  return (
    <Content>
      <Title level={4}>Browse by category</Title>
      <div className="card-container">
        <Tabs type="card">
          <TabPane tab="All" key="1">
            <List
              grid={{ gutter: 16, column: 5 }}
              pagination={{
                onChange: (page) => {
                  console.log(page);
                },
                pageSize: 10,
                size: "small",
                position: "top",
                showSizeChanger: false,
              }}
              dataSource={movies}
              renderItem={(item) => (
                <List.Item>
                  <Link to={`/${item.id}`}>
                    <Card
                      hoverable
                      style={{ width: 190 }}
                      cover={
                        <img
                          alt="example"
                          src={item.poster}
                          // style={{ height: 250 }}
                        />
                      }
                    >
                      <Meta
                        title={item.title}
                        description={item.release_date}
                      />
                    </Card>
                  </Link>
                </List.Item>
              )}
            />
          </TabPane>
          <TabPane tab="Action" key="2">
            <List
              grid={{ gutter: 16, column: 5 }}
              pagination={{
                onChange: (page) => {
                  console.log(page);
                },
                pageSize: 10,
                size: "small",
                position: "top",
                showSizeChanger: false,
              }}
              dataSource={movies.filter((item) => item.genre === "Action")}
              renderItem={(item) => (
                <List.Item>
                  <Link to={`/${item.id}`}>
                    <Card
                      hoverable
                      style={{ width: 190 }}
                      cover={
                        <img
                          alt="example"
                          src={item.poster}
                          // style={{ height: 250 }}
                        />
                      }
                    >
                      <Meta
                        title={item.title}
                        description={item.release_date}
                      />
                    </Card>
                  </Link>
                </List.Item>
              )}
            />
          </TabPane>
          <TabPane tab="Thriller" key="3">
            <List
              grid={{ gutter: 16, column: 5 }}
              pagination={{
                onChange: (page) => {
                  console.log(page);
                },
                pageSize: 10,
                size: "small",
                position: "top",
                showSizeChanger: false,
              }}
              dataSource={movies.filter((item) => item.genre === "Thriller")}
              renderItem={(item) => (
                <List.Item>
                  <Link to={`/${item.id}`}>
                    <Card
                      hoverable
                      style={{ width: 190 }}
                      cover={
                        <img
                          alt="example"
                          src={item.poster}
                          // style={{ height: 250 }}
                        />
                      }
                    >
                      <Meta
                        title={item.title}
                        description={item.release_date}
                      />
                    </Card>
                  </Link>
                </List.Item>
              )}
            />
          </TabPane>
          <TabPane tab="Drama" key="4">
            <List
              grid={{ gutter: 16, column: 5 }}
              pagination={{
                onChange: (page) => {
                  console.log(page);
                },
                pageSize: 10,
                size: "small",
                position: "top",
                showSizeChanger: false,
              }}
              dataSource={movies.filter((item) => item.genre === "Drama")}
              renderItem={(item) => (
                <List.Item>
                  <Link to={`/${item.id}`}>
                    <Card
                      hoverable
                      style={{ width: 190 }}
                      cover={
                        <img
                          alt="example"
                          src={item.poster}
                          // style={{ height: 250 }}
                        />
                      }
                    >
                      <Meta
                        title={item.title}
                        description={item.release_date}
                      />
                    </Card>
                  </Link>
                </List.Item>
              )}
            />
          </TabPane>
          <TabPane tab="Animation" key="5">
            <List
              grid={{ gutter: 16, column: 5 }}
              pagination={{
                onChange: (page) => {
                  console.log(page);
                },
                pageSize: 10,
                size: "small",
                position: "top",
                showSizeChanger: false,
              }}
              dataSource={movies.filter((item) => item.genre === "Animation")}
              renderItem={(item) => (
                <List.Item>
                  <Link to={`/${item.id}`}>
                    <Card
                      hoverable
                      style={{ width: 190 }}
                      cover={
                        <img
                          alt="example"
                          src={item.poster}
                          // style={{ height: 250 }}
                        />
                      }
                    >
                      <Meta
                        title={item.title}
                        description={item.release_date}
                      />
                    </Card>
                  </Link>
                </List.Item>
              )}
            />
          </TabPane>
        </Tabs>
      </div>
    </Content>
  );
};

export default MainContent;
