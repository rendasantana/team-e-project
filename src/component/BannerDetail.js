import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMovieDetail } from "../store/actions/moviesAct";
import { Button, Rate, Modal } from "antd";
import "../asset/style/layout/private/_bannerdetail.scss";

const BannerDetail = (props) => {
  const id = props.param;
  const detail = useSelector((state) => state.moviesRed.detail);
  const trailer = useSelector((state) => state.moviesRed.trailer);
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    dispatch(getMovieDetail(id));
  }, [dispatch, id]);

  return (
    <div className="container">
      <Modal
        title={detail.title}
        visible={visible}
        onCancel={() => setVisible(false)}
        width="750px"
        footer={null}
        centered={true}
      >
        <iframe
          title="video"
          src={`https://www.youtube.com/embed/${trailer}`}
          width="700"
          height="400"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        />
      </Modal>

      <div
        className="bannerdetail"
        style={{
          backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)),
        url(${detail.poster})`,
        }}
      >
        <div className="detail">
          <div className="title">{detail.title}</div>
          <div className="ratting">
            <Rate />
            <p>220 Reviews</p>
          </div>
          <div className="desc">
            <p>{detail.overview}</p>
          </div>
          <div className="btn">
            <Button type="primary" onClick={() => setVisible(true)}>
              Watch Trailler
            </Button>
            <Button ghost type="primary">
              Add To Watchlist
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BannerDetail;
