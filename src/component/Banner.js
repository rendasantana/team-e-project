import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import '../asset/style/layout/_banner.scss';

class Banner extends Component {
  render() {
    const settings = {
      dots: true,
      autoplay: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div className="container">
        <div className="banner">
          <Slider {...settings}>
            <div><img src={require('../asset/img/1.jpg')} alt="Credit"/></div>
            <div><img src={require('../asset/img/2.jpg')} alt="Credit"/></div>
            <div><img src={require('../asset/img/3.jpg')} alt="Credit"/></div>
            <div><img src={require('../asset/img/4.jpg')} alt="Credit"/></div>
            <div><img src={require('../asset/img/5.jpg')} alt="Credit"/></div>
            <div><img src={require('../asset/img/6.jpg')} alt="Credit"/></div>
          </Slider>
        </div>
      </div>
      
    );
  }
}

export default Banner;