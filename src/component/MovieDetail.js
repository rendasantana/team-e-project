import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMovieDetail } from "../store/actions/moviesAct";
import {
  Layout,
  Typography,
  Tabs,
  Card,
  Divider,
  Descriptions,
  List,
} from "antd";
import "antd/dist/antd.css";
import "../asset/style/MovieDetail.scss";
// import movie from "../asset/img/fight-club.jpg";
import ReviewSection from "./ReviewSection";

const { Content } = Layout;
const { Title } = Typography;
const { TabPane } = Tabs;
const { Meta } = Card;

const MovieDetail = (props) => {
  const id = props.param;
  const detail = useSelector((state) => state.moviesRed.detail);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMovieDetail(id));
  }, [dispatch, id]);

  const actor = useSelector((state) => state.moviesRed.actor);

  return (
    <Content>
      <Title level={4}>{detail.title}</Title>
      <div className="card-container">
        <Tabs type="card">
          <TabPane tab="Overview" key="1">
            <div className="detail-container">
              <Divider orientation="left">Synopsis</Divider>
              <p>{detail.overview}</p>
            </div>
            <div className="detail-container">
              <Divider orientation="left">Information</Divider>
              <Descriptions>
                <Descriptions.Item label="Release Date">
                  {detail.release_date}
                </Descriptions.Item>
                <Descriptions.Item label="Director">
                  {detail.director}
                </Descriptions.Item>
                <Descriptions.Item label="Distributed by">
                  {detail.production_house}
                </Descriptions.Item>
                <Descriptions.Item label="Country">
                  {detail.country}
                </Descriptions.Item>
                <Descriptions.Item label="Running Time">
                  {detail.running_time} minutes
                </Descriptions.Item>
              </Descriptions>
            </div>
          </TabPane>
          <TabPane tab="Characters" key="2">
            <List
              grid={{ gutter: 16, column: 5 }}
              dataSource={actor}
              renderItem={(item) => (
                <List.Item>
                  <Card
                    hoverable
                    style={{ width: 190 }}
                    // cover={
                    //   <img alt="example" src={movie} style={{ height: 150 }} />
                    // }
                  >
                    <Meta title={item} />
                  </Card>
                </List.Item>
              )}
            />
          </TabPane>
          <TabPane tab="Review" key="3">
            {ReviewSection()}
          </TabPane>
        </Tabs>
      </div>
    </Content>
  );
};

export default MovieDetail;
