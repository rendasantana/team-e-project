import React, { useState } from "react";
import { Form, Input, Button } from "antd";
import Header from "../layout/Header";
import "../asset/style/component/register.scss";
import { useDispatch } from "react-redux";
import { register } from "../store/actions/auth";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const Register = (props) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  // const [error, setError] = useState(false);
  const dispatch = useDispatch();

  // const submit = async(e) => {
  //     e.preventDefault()
  //     console.log("test input")
  //     setIsLoading(true)

  //     const signUser = {
  //         name,
  //         email,
  //         password
  //     }

  //     try {
  //       const res = await axios.post(`${baseUrl}/user/register`, signUser)
  //       console.log("test respon 123", res)
  //       if(res.data.status === "Success") {
  //         localStorage.setItem("token", res.data.data.token)
  //             setName("")
  //             setEmail("")
  //             setPassword("")
  //             setIsLoading(false)
  //         props.history.push("/home")
  //       }
  //     }catch(err){
  //       console.log(err)
  //         setName("")
  //         setEmail("")
  //         setPassword("")
  //         setIsLoading(false)
  //     }
  //   }

  const onFinish = async (value) => {
    console.log("Success:", value);
    setIsLoading(true);

    const signUser = {
      name,
      email,
      password,
    };
    await dispatch(register(signUser));
    props.history.push("/");
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Header />
      <div className="register">
        <h1>Register</h1>
        <div className="container">
          <div className="form-register">
            <Form
              {...layout}
              name="basic"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              {/* Name */}
              <Form.Item
                label="Name"
                name="name"
                rules={[{ required: true, message: "Please input your name" }]}
              >
                <Input
                  name="name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Item>
              {/* Email */}
              <Form.Item
                label="Email"
                name="email"
                rules={[{ required: true, message: "Please input your email" }]}
              >
                <Input
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Form.Item>

              {/* Password */}
              <Form.Item
                label="Password"
                name="password"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input.Password
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Item>

              <Form.Item {...tailLayout}>
                <Button ghost type="primary" outline htmlType="submit">
                  {isLoading ? "loading..." : "Sign Up"}
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Register;
