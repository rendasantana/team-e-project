import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faTwitter,
  faInstagram,
  faPinterestP,
} from "@fortawesome/free-brands-svg-icons";
import "../asset/style/SocialIcon.scss";

function SocialIcon() {
  return (
    <div className="top">
      <ul className="social-media">
        <li>
          <div className="icon facebook">
            <i className="fab" alt="facebook">
              <FontAwesomeIcon icon={faFacebookF} />
            </i>
          </div>
        </li>
        <li>
          <div className="icon instagram">
            <i className="fab" alt="instagram">
              <FontAwesomeIcon icon={faInstagram} />
            </i>
          </div>
        </li>
        <li>
          <div className="icon twitter">
            <i className="fab " alt="twitter">
              <FontAwesomeIcon icon={faTwitter} />
            </i>
          </div>
        </li>
        <li>
          <div className="icon pinterest">
            <i className="fab" alt="pinterest">
              <FontAwesomeIcon icon={faPinterestP} />
            </i>
          </div>
        </li>
      </ul>
    </div>
  );
}

export default SocialIcon;
