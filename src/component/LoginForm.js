import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { login } from "../store/action/auth";

const LoginForm = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();

  // const change = e => {
  //     setUser({
  //         [e.taret.name] : e.target.value
  //     })
  // }

  const submit = (e) => {
    e.preventDefault();
    const userData = {
      email,
      password,
    };
    console.log("data", userData);
    dispatch(login(userData));
    props.history.replace("/");
  };

  return (
    <div>
      <form onSubmit={submit}>
        <input
          type="name"
          value={email}
          name="email"
          placeholder="email"
          // onChange={change}
          onChange={(e) => setEmail(e.target.value)}
        />

        <input
          type="name"
          value={password}
          name="password"
          placeholder="password"
          // onChange={change}
          onChange={(e) => setPassword(e.target.value)}
        />

        <button>login</button>
      </form>
    </div>
  );
};

export default LoginForm;
