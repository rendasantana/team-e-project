import React, { useState } from "react";
import { Button, Form, Input, Checkbox } from "antd";
import { Link } from "react-router-dom";
import "../asset/style/component/login.scss";
import { useDispatch } from "react-redux";
import { login } from "../store/actions/auth";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const Login = (props) => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

    const onFinish = () => {
        console.log('Success:');
        setIsLoading(true)
        const userData = {
            email,
            password
          }
          dispatch(login(userData));
          // props.history.push('/home');
      };
    
    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
        alert("Check Email and Password !")
      };

  return (
    <div className="login">
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <h1>LOGIN USER</h1>

        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Please input your email!" }]}
        >
          <Input
            type="email"
            placeholder="Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Item>

        <Form.Item {...tailLayout} name="remember" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button ghost type="primary" outline htmlType="submit">
            {isLoading ? "loading..." : "Login"}
          </Button>
        </Form.Item>

        <span>or </span>
        <Link to="/register" className="btn-register">
          Register
        </Link>
        <span> Now!!</span>
      </Form>
    </div>
  );
};

export default Login;
