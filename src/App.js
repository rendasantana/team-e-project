import React, { Fragment } from "react";

import "./App.css";
import Routers from "./routers/Routers";
import FooterPage from "./layout/FooterPage";

function App() {
  return (
    <Fragment>
      <Routers />
      <FooterPage />
    </Fragment>
  );
}

export default App;
