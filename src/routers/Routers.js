import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
// import LoginForm from '../component/LoginForm'
import HomePage from "../pages/HomePage";
import Login from "../component/Login";
// import Register from "../component/Register";
// import HomeLogin from "../pages/HomeLogin";
import DetailPage from "../pages/DetailPage";
import UserProfile from "../component/Profile";

export default class Routers extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Switch>
              <Route path="/" exact component={HomePage} />
              <Route path="/:movie" exact component={DetailPage} />
              <Route path="login" component={Login} />
              <Route path="/profile" exact component={UserProfile}/>
              {/* <Route path="/Home" exact component={HomeLogin} /> */}
              {/* <Route path="/login" component={LoginForm} /> */}
              {/* <Route path="/register" exact component={Register} /> */}
            </Switch>
        </BrowserRouter>
      </>
    );
  }
}
