export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILED = "REGISTER_FAILED";
export const LOGOUT = "LOGOUT";
export const SEARCH_MOVIE = "SEARCH_MOVIE";
export const GET_PROFILE = "GET_PROFILE";
export const GET_USER = "GET_USER";
