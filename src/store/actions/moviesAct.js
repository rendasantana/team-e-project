import { SEARCH_MOVIE } from "./types";
import axios from "axios";

export const getAllMovies = () => async (dispatch) => {
  try {
    const res = await axios.get(
      "https://mini-project-02.herokuapp.com/api/v1/movie/show?page=1&limit=60"
    );
    console.log(res.data.data.rows);
    dispatch({
      type: "GET_MOVIES",
      payload: res.data.data.rows,
    });
  } catch (error) {
    console.log(error);
  }
};

export const getMovieDetail = (id) => async (dispatch) => {
  //get movie detail
  try {
    const res = await axios.get(
      `https://mini-project-02.herokuapp.com/api/v1/movie/show/detail/${id}`
    );
    console.log("RES", res.data.data.detail);
    dispatch({
      type: "GET_MOVIE_DETAIL",
      payload: res.data.data.detail, //detail data
      actor: res.data.data.detail.actor.split(",", 20), //actor
      trailer: res.data.data.detail.trailer.replace(
        "https://www.youtube.com/watch?v=",
        ""
      ),
    });
  } catch (error) {
    console.log(error);
  }
};

export const searchMovie = (text) => async (dispatch) => {
  try {
    const res = await axios.get(
      `https://mini-project-02.herokuapp.com/api/v1/movie/show/search-title?page=1&limit=10`
    );
    console.log("RES", res.data.data);
    dispatch({
      type: SEARCH_MOVIE,
      payload: res.data.data.title,
    });
  } catch (error) {
    console.log(error);
  }
};
