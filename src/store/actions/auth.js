import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  REGISTER_FAILED,
  REGISTER_SUCCESS,
  LOGOUT, 
  // GET_USER
} from "./types";
import axios from "axios";
const baseUrl = "https://mini-project-02.herokuapp.com/api/v1";

export const login = (data) => async (dispatch) => {
  try {
    const res = await axios.post(`${baseUrl}/user/login`, data);
    localStorage.setItem("token", res.data.data.token);
    console.log(res);
    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data.data.user,
    });
    console.log("login", res);
  } catch (error) {
    console.log(error.status);

    dispatch({
      type: LOGIN_FAILED,
    });
  }
};

export const register = (data) => async (dispatch) => {
  try {
    const res = await axios.post(`${baseUrl}/user/register`, data);
    localStorage.setItem("token", res.data);
    console.log("register", res);
    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data.data.user,
    });
  } catch (error) {
    console.log(error.status);
    dispatch({
      type: REGISTER_FAILED,
    });
  }
};

export const logout = () => {
  return {
    type: LOGOUT,
  };
};

// export const getProfile = () => async dispatch => {
//   let token = localStorage.getItem("token")
//   try{
//       const res = await axios.get(`${baseUrl}/user/profile`, {
//           headers: {
//               Authorization: token
//           }
//       })
//       console.log('getprofile', res.data.user)
//       dispatch({
//           type: GET_PROFILE,
//           payload: res.data.data
//       })
//   }catch(error){
//       console.log(error, error.response)
//   }
// }
