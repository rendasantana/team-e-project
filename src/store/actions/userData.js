import { GET_PROFILE } from "./types";
import axios from "axios";
const baseUrl = "https://mini-project-02.herokuapp.com/api/v1";

export const getProfile = () => async (dispatch) => {
  let token = localStorage.getItem("token");
  try {
    const res = await axios.get(`${baseUrl}/user/profile`, {
      headers: {
        Authorization: token,
      },
    });
    console.log("getprofile", res);
    dispatch({
      type: GET_PROFILE,
      payload: res.data.data.owner,
    });
  } catch (error) {
    console.log(error, error.response);
  }
};

