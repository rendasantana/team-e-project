import { GET_PROFILE } from "../actions/types";

const initialState = {
  user: {},
};

const userData = (state = initialState, action) => {
  switch (action.type) {
    default:
      return {
        ...state,
      };
    case GET_PROFILE:
      return {
        ...state,
        user: action.payload,
      };
  }
};

export default userData;

