import { combineReducers } from "redux";
import auth from "./auth";
import moviesRed from "./moviesRed";
import userData from "./userData";

export default combineReducers({
  auth,
  userData,
  moviesRed,
});
