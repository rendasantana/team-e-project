const initialState = {
  movies: [],
  detail: {},
  actor: "",
  trailer: "",
  title: "",
  movie: [],
};

const moviesRed = (state = initialState, action) => {
  switch (action.type) {
    default:
      return {
        ...state,
      };
    case "GET_MOVIES":
      return {
        ...state,
        movies: action.payload,
      };
    case "GET_MOVIE_DETAIL":
      return {
        ...state,
        detail: action.payload,
        actor: action.actor,
        trailer: action.trailer,
      };
    case "SEARCH_MOVIE":
      return {
        ...state,
        movies: action.payload,
        title: action.title,
      };
  }
};

export default moviesRed;
