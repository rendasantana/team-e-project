import { LOGIN_SUCCESS, LOGIN_FAILED, REGISTER_SUCCESS, REGISTER_FAILED, LOGOUT } from "../actions/types";
const initialState = {
  token: localStorage.getItem("token"),
  error: null,
  isAuthenticate: false, 
}

const auth = (state = initialState, action) => {
  switch(action.type) {
    default:
      return {
        ...state
      }
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticate:true
      }
    case LOGIN_FAILED:
      return {
        ...state,
        isAuthenticate:false,
        token: localStorage.removeItem("token")
      }
    case REGISTER_SUCCESS:
      return {
        ...state,
        isAuthenticate:true
      }
    case REGISTER_FAILED:
      return {
        ...state,
        isAuthenticate:false,
        token: localStorage.removeItem("token")
      }
    case LOGOUT:
      localStorage.clear();
      return {
        ...state,
        token: null
      }
    
  }
}

export default auth;
