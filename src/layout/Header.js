import React, { useEffect, useState } from "react";
import { Input, Button, Modal, Menu, Form, Dropdown, Checkbox } from "antd";
import {
  UserOutlined,
  MailOutlined,
  LockOutlined,
  DownOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import "../asset/style/layout/_header.scss";

import { getProfile } from "../store/actions/userData";
import { login, logout, register } from "../store/actions/auth";
import { useDispatch, useSelector } from "react-redux";

const { Search } = Input;

const Header = () => {
  const dispatch = useDispatch();
  const token = localStorage.getItem("token");
  const [modalLogin, setModalLogin] = useState(false);
  const [modalRegister, setModalRegister] = useState(false);
  const isAuthenticate = useSelector((state) => state.auth.isAuthenticate);

  //ngambil user datanya liat dari redux devtool
  const userdata = useSelector((state) => state.userData.user);

  const [input, setInput] = useState({
    email: "",
    password: "",
  });

  useEffect(() => {
    // dicek pake token kalo ada fungsi getprofil jalan
    if (token) {
      dispatch(getProfile());
    }
  }, [dispatch, token]);

  const handleInput = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const handleLogin = (props) => {
    dispatch(login(input));
    setModalLogin(false);
  };

  const handleRegister = (props) => {
    dispatch(register(input));
    setModalRegister(false);
  };

  const handleSignOut = () => {
    dispatch(logout());
    window.location.reload();
  };

  // Menu Profile Login
  const ProfileMenu = (
    <Menu>
      <Menu.Item key="1">Profile</Menu.Item>
      <Menu.Item key="2">Settings</Menu.Item>
      <Menu.Item key="3">Help</Menu.Item>
      <Menu.Item key="4" onClick={handleSignOut}>
        Logout
      </Menu.Item>
    </Menu>
  );

  // Modal for Login
  const LoginModal = (
    <Modal
      title="Login"
      okText="Login"
      visible={modalLogin}
      onOk={() => handleLogin()}
      onCancel={() => setModalLogin(false)}
      cancelButtonProps={{ style: { display: "none" } }}
    >
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
      >
        <Form.Item
          name="email"
          rules={[{ required: true, message: "Please input your email!" }]}
        >
          <Input
            name="email"
            prefix={<MailOutlined className="site-form-item-icon" />}
            placeholder="Email"
            onChange={handleInput}
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Please input your Password!" }]}
        >
          <Input
            name="password"
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
            onChange={handleInput}
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>
        </Form.Item>
        Don't have an account yet?{" "}
        <span 
        style={{color:"#1890ff", cursor:"pointer"}}
        onClick={() => setModalLogin(false) & setModalRegister(true)}>
          register now!
        </span>
      </Form>
    </Modal>
  );

  // modal for register
  const RegisterModal = (
    <Modal
      title="Register"
      okText="Register"
      visible={modalRegister}
      onOk={() => handleRegister()}
      onCancel={() => setModalRegister(false)}
      cancelButtonProps={{ style: { display: "none" } }}
    >
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
      >
        <Form.Item
          name="name"
          rules={[{ required: true, message: "Please input your name!" }]}
        >
          <Input
            name="name"
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Username"
            onChange={handleInput}
          />
        </Form.Item>
        <Form.Item
          name="email"
          rules={[{ required: true, message: "Please input your email!" }]}
        >
          <Input
            name="email"
            prefix={<MailOutlined className="site-form-item-icon" />}
            placeholder="Email"
            onChange={handleInput}
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Please input your Password!" }]}
        >
          <Input
            name="password"
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
            onChange={handleInput}
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>
        </Form.Item>
        Already have login and password?{" "}
        <span 
        style={{color:"#1890ff", cursor:"pointer"}}
        onClick={() => setModalRegister(false) & setModalLogin(true)}>
          Sign in
        </span>
      </Form>
    </Modal>
  );

  return (
    <>
      <div className="container">
        <div className="container-header">
          <div className="header">
            <div className="header logo">
              <a href="/">
                <img src={require("../asset/img/king.png")} alt="logo" />
              </a>
              <p>
                Juragan <span>Video</span>
              </p>
            </div>

            {/* part Search */}
            <div className="header src">
              <Search
                type="text"
                placeholder="Search..."
                // onChange={onChange}
                // onSearch={onSubmit}
                style={{ height: 30, width: 500 }}
              />
            </div>

            {/* part button login */}
            <div className="header btn">
              {token || isAuthenticate ? (
                <div className="header profile">
                  <Dropdown overlay={ProfileMenu}>
                    <Link to=""
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      <img src={require("../asset/img/king.png")} alt="logo" />
                      <DownOutlined className="dop"/>
                      <p className="user-name"> Hy, {userdata.name} !!</p>
                    </Link>
                  </Dropdown>
                </div>
              ) : (
                <Button
                  ghost
                  type="primary"
                  onClick={() => setModalLogin(true)}
                >
                  Login
                </Button>
              )}
            </div>
            {LoginModal}
            {RegisterModal}
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
