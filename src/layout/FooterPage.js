import React from "react";
import { Layout, Typography, Col, Row, Space, Divider } from "antd";
import SocialIcon from "../component/SocialIcon";
import "antd/dist/antd.css";
import "../asset/style/FooterPage.scss";
import google from "../asset/img/google-play-badge.png";
import apple from "../asset/img/apple_store.svg";
const { Footer } = Layout;
const { Title, Text, Paragraph } = Typography;

const FooterPage = () => {
  return (
    <Footer>
      <Row>
        <Col span={10}>
          <Title>LOGO</Title>
          <Paragraph>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aspernatur
            nulla sunt blanditiis nisi natus officiis quos impedit nesciunt,
            maiores distinctio, deserunt porro eveniet quisquam reiciendis
            tempora. Voluptatibus ut eligendi facere.
          </Paragraph>
        </Col>
        <Col span={5} offset={2}>
          <Space direction="vertical">
            <Text strong="true">Tentang Kami</Text>
            <Text strong="true">Blog</Text>
            <Text strong="true">Layanan</Text>
            <Text strong="true">Karir</Text>
            <Text strong="true">Pusat Media</Text>
          </Space>
        </Col>
        <Col span={5}>
          <Space direction="vertical" style={{ width: 252.5 }}>
            <Text strong="true">Download</Text>
            <div className="download-from">
              <div className="store">
                <img className="google" src={google} alt="" />
              </div>
              <div className="store">
                <img className="apple" src={apple} alt="" />
              </div>
            </div>
            <Text strong="true">Social Media</Text>
            <SocialIcon />
          </Space>
        </Col>
      </Row>
      <Divider style={{ borderTop: "1px solid rgb(0,0,0,0.1)" }} />
      <Paragraph className="copyright">
        Copyright © 2020 TIM-E. All Rights Reserved
      </Paragraph>
    </Footer>
  );
};

export default FooterPage;
