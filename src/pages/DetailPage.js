import React, { Fragment } from "react";
import MovieDetail from "../component/MovieDetail";
import Header from "../layout/Header";
import BannerDetail from "../component/BannerDetail";

const DetailPage = (props) => {
  const id = props.match.params.movie;

  return (
    <Fragment>
      <Header />
      <BannerDetail param={id} />
      <MovieDetail param={id} />
    </Fragment>
  );
};

export default DetailPage;
