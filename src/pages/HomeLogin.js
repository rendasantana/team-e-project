import React, { Component } from "react";
import Banner from "../component/Banner";
import "../asset/style/component/_homepage.scss";
import MainContent from "../component/MainContent";
import HeaderLogin from '../layout/HeaderLogin';

export default class HomeLogin extends Component {
  render() {
    return (
      <>
        <div className="home">
            <HeaderLogin />
            <Banner />
            <MainContent />
        </div>
      </>
    );
  }
}
