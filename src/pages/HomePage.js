import React, { Component } from "react";
import Banner from "../component/Banner";
import "../asset/style/component/_homepage.scss";
import MainContent from "../component/MainContent";
import Header from "../layout/Header";

export default class HomePage extends Component {
  render() {
    return (
      <>
        <div className="home">
          <Header props={this.props} />
          <Banner />
          <MainContent />
        </div>
      </>
    );
  }
}
